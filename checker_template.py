import random


class CheckerTemplate:
    # has `self` since checkers *should* have instances
    # it's not actually needed here though since it's just static methods
    def __init__(self, arguments):
        pass

    def get_status(self):
        latency = int(random.random() * 3000)
        if latency > 2500:  # "simulate" a timeout
            return 0
        elif latency > 1000:  # "simulate" a very slow application - a partial outage
            return 50
        else:
            return 100

    def get_return_codes(self):
        return {0: "Down", 50: "Partial outage", 100: "Up"}
